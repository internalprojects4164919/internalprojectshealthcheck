const { defineConfig } = require("cypress");

module.exports = defineConfig({
  projectId: 'hbcckh',
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here

    },
    "reporter": "mochawesome",
    "reporterOptions": {
    "charts": true,
    "overwrite": false,
    "html": false,
    "json": true,
    "reportDir": "mochawesome-report"
  },

    //baseUrl: 'https://fe-test.tremium.net/',
    defaultCommandTimeout: 20000,
    scrollBehavior: 'nearest',
    viewportWidth: 1024,
    viewportHeight: 600,
    video:true

  },
});
