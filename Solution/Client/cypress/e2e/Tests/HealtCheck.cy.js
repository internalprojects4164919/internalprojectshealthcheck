/// <reference types="cypress" />
/// <reference types="cypress/xpath" />
import LogInPageFE from "../../Pages/LogInPageFE.js"
import LogInPageIM from "../../Pages/LogInPageInspectM.js"
import SidebarFE from "../../Pages/SidebarFE.js"
import DashboardInspectM from"../../Pages/DashboardInspectM"
import LogInPageInvoiceM from "..//../Pages/LogInPageInvoiceM.js"
import HomePageTremiumSite from "../../Pages/HomePageTremiumSite.js"


describe('Log in ', () => {

    beforeEach(() => {
       
    
        })


        it('Login to Invoice manager',  () =>
        {
            
            // Dodajemo Cypress konfiguraciju da ignoriše neuhvaćene izuzetke
                Cypress.on('uncaught:exception', (err, runnable) => {
               
                return false;
            });
      
            LogInPageInvoiceM.visitLogInPage()
            cy.url().should('eq', 'https://im.tremium.net/Identity/Account/Login')

            LogInPageInvoiceM.logInUser("vuckovic.jelena@gmail.com", '123456')

            cy.url().should('eq',"https://im.tremium.net/Identity/Account/Login" )

            cy.wait(1000)
            LogInPageInvoiceM.errorMessage().should('contain.text', "Invalid login attempt.")
            
            
        })

    

    it('Login to Financial Engine', () => {

        LogInPageFE.visitLogInPage()
        cy.url().should('eq', 'https://fe.tremium.net/#/sign-in')
        LogInPageFE.logInUser("test.tremium@yopmail.com", '123456')
        cy.url().should('eq', 'https://fe.tremium.net/#/dashboard')
        LogInPageFE.findSplashScreen().should('not.exist')
        cy.wait(2000)
        SidebarFE.getRolemanagementMainLink().should('be.visible')


    })

    it('Login to Inspect Master', () => {

        LogInPageIM.visitLogInPage()
        cy.url().should('eq', 'https://ec.tremium.net/login')

        LogInPageIM.logInUser("vuckovic.jelena@gmail.com", 'User!123')
        cy.url().should('eq',"https://ec.tremium.net/home" )
        cy.wait(2000)
        
        DashboardInspectM.getDashboardHeadline().should('contain.text', "Dobrodošli na vašu kontrolnu tablu!")


    })

    it('Navigate trough pages on Tremium Site', () => {

        HomePageTremiumSite.visitHomePage()
        cy.url().should('eq', 'https://tremium.net/')
        
        HomePageTremiumSite.getCareersButton().should('be.visible').click()
        cy.wait(1000)
        cy.url().should('eq', 'https://tremium.net/careers')

        HomePageTremiumSite.getContactButton().should('be.visible').click()
        cy.wait(1000)
        cy.url().should('eq', 'https://tremium.net/careers#kod')
        

    })

 
})