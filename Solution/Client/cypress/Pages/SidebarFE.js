class MainMenuPage {


    static getMainMenuList() {
        return cy.xpath("//ul[@role='menu']/li")


    }

    static getNavBarMainHeader() {
        return cy.get('.m-0')
    }

    static getMainMenuAllMembers() {
        return cy.xpath("//ul[@role='menu']/li")
    }


    static getSpinner() {
        return cy.get('#spinnerModal')
    }

    static getSpinnerOnModal()
    {
        return cy.get('#spinnerModal > .modal-dialog > .modal-content > .modal-body')
    }

    static getDashboardlink() {

        return cy.xpath("//*[@data-cy='sidebar-menu_Dashboard']")

    }
    static getAccountBalancesSubLink() {

        return cy.xpath("//*[@data-cy='sidebar-sub_Account Balances']")

    }

    static getAccountBalancesChartSubLink() {

        return cy.xpath("//*[@data-cy='sidebar-sub_Account Balances Chart']")

    }

    static getTransactionsSubLink() {

        return cy.xpath("//*[@data-cy='sidebar-sub_Transactions']")

    }


    static getAccountsMainlink() {

        return cy.xpath("//*[@data-cy='sidebar-menu_Accounts']")

    }

    static getEventsMainlink() {

        return cy.xpath("//*[@data-cy='sidebar-menu_Events']")

    }



    static getDomainsMainlink() {

        return cy.xpath("//*[@data-cy='sidebar-menu_Domains']")

    }

    static getDomainBalancesSublink() {

        return cy.xpath("//*[@data-cy='sidebar-sub_Domain Balances']")

    }

    static getDomainBalancesChartSublink() {

        return cy.xpath("//*[@data-cy='sidebar-sub_Domain Balances Chart']")

    }

    static getColumnsForDisplayButton() {
        return cy.xpath("//button[@data-cy='button_Columns']")
    }

    static getCheckboxTableColumns() {

        return cy.xpath("//cmp-table-columns//input[@type='checkbox']")
    }

    static getColumnNameTableColumns() {
        return cy.xpath("//cmp-table-columns//tr/td[1]")
    }

    static getApplyButtonTableColumns()
    {
        return cy.xpath("//button[@data-cy='columns-button_Search']")
    }


    static chooseColumnsForDisplayByCaption(caption1, caption2, caption3, caption4, caption5) {
        MainMenuPage.getCheckboxTableColumns().click({ multiple: true, force: true }) //force:true Label covers checkbox
        MainMenuPage.getColumnNameTableColumns()

            .each(($el, index, $list) => {
                let name = $el.text()
                cy.log(name)
                if (name == caption1 || name == caption2 || name == caption3 || name == caption4 || name == caption5) {
                    MainMenuPage.getCheckboxTableColumns().eq(index).click({ force: true })
                }
            }

            ).then(() => {
                MainMenuPage.getApplyButtonTableColumns().click()
                cy.wait(1000)
            })
    }

    static getDomainsSubLink() {
        return cy.xpath("//*[@data-cy='sidebar-sub_Domains']")
    }


    static getSettingsMainlink() {

        return cy.xpath("//*[@data-cy='sidebar-menu_Settings']")

    }

    static getUserManagementlink() {

        return cy.xpath("//*[@data-cy='sidebar-menu_User Management']")

    }

    static getUserIconButton() {
        return cy.xpath("//*[@class='initial-avatar change-pointer mr-3 mr-md-5']")
    }

    static getLogoutButton() { return cy.contains('Log Out') }



    static getAccountTypesLink() {
        return cy.xpath("//*[@data-cy='sidebar-sub_Account Types']")

    }
    static getSubAccountsLink() {
        return cy.xpath("//*[@data-cy='sidebar-sub_Accounts']")
    }

    static getSubmitEventLink() {
        return cy.xpath("//*[@data-cy='sidebar-sub_Submit Event']")
    }

    static getSubEventsLink() {

        return cy.xpath("//*[@data-cy='sidebar-sub_Events']")
    }

    static getTargetAccountLink() {

        return cy.xpath("//*[@data-cy='sidebar-sub_Target Account']")
    }

    static getEventTypesLink() {

        return cy.xpath("//*[@data-cy='sidebar-sub_Event Types']")
    }
    static getTagsLink() {

        return cy.xpath("//*[@data-cy='sidebar-menu_Tags']")

    }

    static getRolemanagementMainLink() {

        return cy.xpath("//*[@data-cy='sidebar-menu_Role Management']")

    }
    static getRelatedPermissionsLink() {

        return cy.xpath("//*[@data-cy='sidebar-sub_Related Permissions']")

    }

    static getRolemanagementSubLink() {

        return cy.xpath("//*[@data-cy='sidebar-sub_Role Management']")

    }

    static getPopUpMessage() {

        //return cy.get('.ng-trigger > .ng-tns-c31-3') }
        //cy.xpath("//div[@role='alert']")}

        return cy.xpath("//*[@class='overlay-container']/div/div/div")

    }


}


module.exports = MainMenuPage