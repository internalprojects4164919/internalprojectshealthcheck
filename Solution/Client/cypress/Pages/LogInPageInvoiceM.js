class LogInPageInvoiceM {

    static visitLogInPage(){

        cy.visit("https://im.tremium.net/Identity/Account/Login")

    }
  

    static getEmailInput()
    {
        return cy.xpath("//input[@type='email']")

    }

    static typeEmail(email){

        this.getEmailInput().type(email)

    }

    static getPasswordInput()
    {
        return cy.xpath("//input[@type='password']")
    }

    static typePassword(password){

        this.getPasswordInput().type(password)
    }


    static getLoginButton(){

        return cy.xpath("//button[@type='submit']")

    }

    

    static logInUser(userMail, userPassword)
    {
        this.typeEmail(userMail)
        this.typePassword(userPassword)
        this.getLoginButton().click()
    }

    static errorMessage ()
    {
        return cy.xpath("//li")
    }

    
}

module.exports = LogInPageInvoiceM