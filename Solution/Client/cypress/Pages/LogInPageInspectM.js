const { v4: uuidv4 } = require('uuid');
class LogInPage {

    static visitLogInPage(){

        cy.visit("https://ec.tremium.net/login")
        

    }
  

    static getEmailInput()
    {
        return cy.xpath("//input[@data-qa='email-input']")

    }

    static typeEmail(email){

        this.getEmailInput().type(email)

    }

    static getPasswordInput()
    {
        return cy.xpath("//input[@data-qa='password-input']")
    }

    static typePassword(password){

        this.getPasswordInput().type(password)
    }


    static getLoginButton(){

        return cy.xpath("//*[@data-qa='login-submit-button']")

    }

    

    static logInUser(userMail, userPassword)
    {
        this.typeEmail(userMail)
        this.typePassword(userPassword)
        this.getLoginButton().click()
    }

    
}



module.exports=LogInPage